function update() {
    var refresh = 1000; // Refresh rate in milli seconds
    setTimeout('displayCurrentTime()', refresh);
    setTimeout('displayTimeInWords()', refresh);
};

function displayCurrentTime() {
    var currentTime = moment().format('HH:mm');
    var eDisplayMoment = document.getElementById('displayDate');
	
    eDisplayMoment.innerHTML = currentTime;
    update();
};

function displayTimeInWords(hour, minutes){
    console.log(window.innerWidth)
    var currentMinutes = moment().format('m');
    var currentHours = moment().format('h');
	var wordArray = document.getElementsByClassName('word');
    var minuteArray = document.getElementsByClassName('minutes');

    for(var i = 0; i < wordArray.length; i++){
        wordArray[i].className = wordArray[i].className.replace(" active", "");
    }

    for(var i = 0; i < minuteArray.length; i++){
        minuteArray[i].className = minuteArray[i].className.replace(" active", "");
    }

    var minuteItems = getMinutes(currentMinutes);

    for(var i = 0; i < minuteArray.length; i++){
        for(var j = 0; j < minuteItems.length; j++){
            if(minuteItems[j] == 'dot' + i){
                minuteArray[i].className += " active";
            }
        }
    }

    showActiveHours(getHours(currentHours, currentMinutes), wordArray);
	showActiveMinutes(minuteItems, wordArray);
}

function showActiveHours(hourItems, wordArray){
    for(var i = 0; i < wordArray.length; i++){
        for(var j = 0; j < hourItems.length; j++){
            if(wordArray[i].id == hourItems[j]){
                if(wordArray[i].className.indexOf("active") === -1) {
                    wordArray[i].className += " active";
                }
            }
        }
    }
}

function showActiveMinutes(minuteItems, wordArray){
    for(var i = 0; i < wordArray.length; i++){
        for(var j = 0; j < minuteItems[0].length; j++){
            if(wordArray[i].id == minuteItems[0][j]){
                if(wordArray[i].className.indexOf("active") === -1) {
                    wordArray[i].className += " active";
                }
            }
        }
    }
}

function getHours(hour, minutes){

    var result = [];
    var map = {
        1:  'one',
        2:  'two',
        3:  'three',
        4:  'four',
        5:  'five2',
        6:  'six',
        7:  'seven',
        8:  'eight',
        9:  'nine',
        10:  'ten2',
        11:  'eleven',
        12:  'twelve'
    };

    if(minutes > 4) {
        var tense = undefined;

        if(minutes > 34){
            tense = 'to';
			
			if(hour == 12){
				hour = 1;
			} else {
				hour++;
			}
        } else {
            tense = 'past';
        }

        result.push(tense);
    }

    result.push(map[hour]);
    return result;
}

function getMinutes(minutes, remainders){
    var result = [];
    var map = {
        '0-4': ['o\'clock'],
        '5-9,55-59': ['five'],
        '10-14,50-54': ['ten'],
        '15-19,45-49': ['quarter'],
        '20-24,40-44': ['twenty'],
        '25-29,35-39': ['twenty', 'five'],
        '30-34': ['half']
    };

    for(var timespan in map){
        var items = map[timespan];
        var rangeArr = timespan.split(',');

        for(var i=0; i < rangeArr.length; i++){
            var range = rangeArr[i].split('-');
			
            if(minutes >= parseInt(range[0]) && minutes <= parseInt(range[1])){
                result.push(items);

                if(minutes > range[0]){
                    var remainder = minutes - range[0];
                    for(var j=0; j < remainder; j++){
                        result.push('dot' + j);
                    }
                }
            }
        }
    }

    return result;
}